account_ids = {
    shared = "181437319056"
}

vpc_cidr            = "200.0.0.0/16"
public_subnetcidr   = ["200.0.0.0/24", "200.0.2.0/24", "200.0.4.0/24"]
private_subnetcidr  = ["200.0.1.0/24", "200.0.3.0/24", "200.0.5.0/24"]
database_subnetcidr = ["200.0.51.0/24", "200.0.53.0/24", "200.0.55.0/24"]
instance_type       = "t2.micro"
instance_class      = "db.t2.micro"
username            = "kojitechs"

dns_name                  = "familybeyondcloud.com"
subject_alternative_names = ["*.familybeyondcloud.com"]
